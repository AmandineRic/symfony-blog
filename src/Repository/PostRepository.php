<?php
namespace App\Repository;

use App\Entity\Post;

class PostRepository
{
    private $pdo;

    public function __construct() {
       
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * 
     * @return post[] 
     */
    public function findAll(): array
    {
        
        $query = $this->pdo->prepare('SELECT * FROM post');
       
        $query->execute();
        
        $results = $query->fetchAll();
        $list = [];
        
        foreach ($results as $line) {
            
            $post = $this->sqlToPost($line);
           
            $list[] = $post;
        }
        
        return $list;
    }
    
    public function add(Post $post): void {
       
        $query = $this->pdo->prepare('INSERT INTO post (title, author, content) VALUES (:title,:author,:content)');
        
        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);
        
        $query->execute();
       
        $post->setId(intval($this->pdo->lastInsertId()));
    }

    public function delete(int $id) :void{
        $query = $this->pdo->prepare('DELETE FROM post WHERE id = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }

    public function modify($title, $author, $content, $id): void {
       
        $query = $this->pdo->prepare('UPDATE post SET title = :title, author = :author, content = :content  WHERE id = :id');
        
        $query->bindValue(':title', $title, \PDO::PARAM_STR);
        $query->bindValue(':author', $author, \PDO::PARAM_STR);
        $query->bindValue(':content',$content , \PDO::PARAM_STR);
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        
        $query->execute();
       
    }

    
    public function findById(int $id): ?Post {
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');
        
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if($line) {
            return $this->sqlToPost($line);
            
        }

        return null;

    }
    
    private function sqlToPost(array $line):Post {
        return new Post($line['title'], $line['author'], $line['postDate'], $line['content'], $line['id']);
    }
    
}