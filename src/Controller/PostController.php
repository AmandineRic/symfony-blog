<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PostController extends AbstractController {
    
    /**
     * @Route("", name="home")
     */
    public function index(PostRepository $repo) {
        $post = $repo->findAll();
        return $this->render('post.html.twig', [
            'post' => $post
        ]);
    }
    /**
     * @Route("/post/{id}", name="id")
     */
    public function onePost(int $id, PostRepository $repo){
        
        $post = $repo->findById($id);
    
        return $this->render('post-id.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route("/addpost", name="addpost")
     */
   public function addPost(Request $request)
    {

            $post = null;
            $title = $request->get("title");
            $author = $request->get("author");
            $content = $request->get("content");
            if ($title && $author && $content) {

                $post = new Post($title, $author, $content);
                $postRepository = new PostRepository();
                $postRepository->add($post);
            }

            return $this->render('form.add.html.twig', [
                'post' => $post
            ]);
    }
    
    /**
    * @Route("/del/{id}", name="deletepost")
    */
   public function deletePost(int $id)
   {
      $repo = new PostRepository();
      $repo->delete($id);
      return $this->redirectToRoute('home');
   }

   /**
    * @Route("/up/{id}", name="update_post")
    */
   public function modify(int $id, Request $request)
   {
      $repo = new PostRepository();
      //On récupère les valeurs des différents input en utilisant la request
      $author = $request->get("author");
      $title = $request->get("title");
      $content = $request->get("content");
      //On vérifie que chaque input contenait bien une valeur
      if ($author && $title && $content) {
         $repo->update($title,$author,$content,$id);
      }
      $postView = $repo->findById($id);

      return $this->render('modify.html.twig', ['postView' => $postView]);
   }
}