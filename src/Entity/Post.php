<?php
namespace App\Entity;

class Post {
    private $id;
    private $title;
    private $author;
    private $content;
    private $postDate;
    

    public function __construct(string $title, string $author,  string $content, string $postDate = " ", int $id = null) {
        $this->id = $id;
        $this->title = $title;
        $this->author= $author;
         $this->content = $content;
         $this->postDate = $postDate;
       
    }

    public function getTitle():string {
        return $this->title;
    }
    public function getAuthor():string {
        return $this->author;
    }
    public function getPostDate():string {
        return $this->postDate;
    }
    public function getContent(): string {
        return $this->content;
    }

    public function getId():int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

}